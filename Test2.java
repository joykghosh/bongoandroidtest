/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testproj;

/**
 *
 * @author joyghosh
 */
public class Test2 {
    
    public static void main(String[] args){
        
        Car c= new Car();
        c.has_gas();
        c.set_num_of_passengers();
        c.set_num_of_wheels();
        
        Plane p = new Plane();
        p.has_gas();
        p.set_num_of_passengers();
        p.set_num_of_wheels();
        
    }
    
    
}

class Car implements Vehicle{

    @Override
    public int set_num_of_wheels() {
        System.out.println("testproj.Car.set_num_of_wheels()");
        return 0;
    }

    @Override
    public int set_num_of_passengers() {
        System.out.println("Car.set_num_of_passengers()");
        return 0;
    }

    @Override
    public boolean has_gas() {
        System.out.println("Car.has_gas()");
        return false;
    }
    
}

class Plane implements Vehicle{

    @Override
    public int set_num_of_wheels() {
        System.out.println("testproj.plane.set_num_of_wheels()");
        return 0;
    }

    @Override
    public int set_num_of_passengers() {
        System.out.println("plane.set_num_of_passengers()");
        return 0;
    }

    @Override
    public boolean has_gas() {
        System.out.println("plane.has_gas()");
        return false;
    }
    
}

interface Vehicle {
int set_num_of_wheels();
int set_num_of_passengers();
boolean has_gas();
}
