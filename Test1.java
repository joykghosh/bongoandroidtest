/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testproj;

import java.util.Arrays;

/**
 *
 * @author joyghosh
 */
public class Test1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       String a ="eat";
       String b = "tar";
       System.out.println(isAnagram(a,b));
    }
    
    static boolean isAnagram(String a,String b){
        
        char[] s1 = a.toLowerCase().toCharArray();
        char[] s2 = b.toLowerCase().toCharArray();
        
        Arrays.sort(s1);
        Arrays.sort(s2);
        
        if(Arrays.equals(s1, s2))
            return true;
       
        return false;
    }
    
}
